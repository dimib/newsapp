//
//  NewsFeedCatalogTests.swift
//  NewsTests
//
//  Created by Dimitri Brukakis on 04.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import XCTest

class NewsFeedCatalogTests: XCTestCase {

    func testNewsFeedCatalog() throws {
        do {
            let catalog = try NewsFeedCatalog.decode(from: json1)
            
            catalog.feeds.publisher.sink { (feed) in
                print(feed)
            }
            
            print(catalog)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    func testNewsFeedCatalogFromBundle() throws {
        
        if let filepath = Bundle.main.path(forResource: "NewsFeedCatalog", ofType: "json") {
            do {
                let content = try String(contentsOfFile: filepath)
                let catalog = try NewsFeedCatalog.decode(from: content)
                catalog.feeds.publisher.sink { (feed) in
                    print(feed)
                }
            } catch {
                XCTFail(error.localizedDescription)
            }
        }
    }
}

let json1 =
"""
{
    "feeds": [
        { "type": "rss", "url": "https://www.zdf.de/rss/podcast/video/zdf/nachrichten/heute-journal", "private": false },
        { "type": "rss", "url": "https://www.zdf.de/rss/podcast/audio/zdf/nachrichten/heute-19-uhr", "private": false },
        { "type": "rss", "url": "https://www.zdf.de/rss/zdf/nachrichten", "private": false }
    ]
}
"""
