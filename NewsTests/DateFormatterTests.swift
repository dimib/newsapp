//
//  DateFormatterTests.swift
//  NewsTests
//
//  Created by Dimitri Brukakis on 05.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import XCTest

class DateFormatterTests: XCTestCase {
    
    

    func testParse() throws {
        let pubDate = "Sat, 04 Apr 2020 13:18:00 +0200"
        
        let dateFormatter = DateFormatter()
        let date = dateFormatter.parsePubDate(string: pubDate)
        XCTAssert(date != nil, "Date is nil")
        print("🕰 \(String(describing: date))")
        
        guard let d = date else { return }
        let cal = Calendar.current
        print("year: \(cal.component(.year, from: d))")
    }
    
    func testFormat() throws {
        let dateFormatter = DateFormatter()
        print("🕰 \(dateFormatter.formatPubDate(date: Date()))")
    }

}
