//
//  RSSFeedDecoderTests.swift
//  NewsTests
//
//  Created by Dimitri Brukakis on 01.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import XCTest

class RSSFeedDecoderTests: XCTestCase {

    func testDecodeRSSFeed() throws {
        let data = rss0.data(using: .utf8)
        do {
            let rss2x = try RSSFeed.decode(from: data!)
            print(rss2x)
            XCTAssert(rss2x.channel?.items != nil)
        } catch {
            XCTFail(error.localizedDescription)
            print(error)
        }
    }
    
    func testDecodeRSSFeedFromBundle() throws {
        print(try loadFromBundle(resource: "heute-19-uhr"))
        print(try loadFromBundle(resource: "heute-journal"))
        print(try loadFromBundle(resource: "zdf-nachrichten"))
    }
    
    private func loadFromBundle(resource: String) throws -> RSSFeed {
        if let filepath = Bundle.main.path(forResource: resource, ofType: "xml") {
            do {
                let content = try String(contentsOfFile: filepath)
                let feed = try RSSFeed.decode(from: content)
                return feed
            } catch {
                throw error
            }
        } else {
            throw NewsError.notFound
        }
    }
}


let rss0: String =
"""
<?xml version="1.0" encoding="UTF-8"?>
<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">
    <channel>
        <title>heute journal (VIDEO)</title>
        <link>https://www.zdf.de/rss/podcast/video/zdf/nachrichten/heute-journal</link>
        <language>de-de</language>
        <copyright>Copyright Zweites Deutsches Fernsehen, ZDF</copyright>
        <itunes:subtitle></itunes:subtitle>
        <itunes:author>ZDFde</itunes:author>
        <itunes:summary>Politische Berichte, scharfsinnige Analysen, verst&#228;ndliche Erkl&#228;rungen </itunes:summary>
        <itunes:owner>
            <itunes:name>ZDF</itunes:name>
            <itunes:email>webmaster@zdf.de</itunes:email>
        </itunes:owner>
        <itunes:explicit>no</itunes:explicit>
        <itunes:image href="http://module.zdf.de/podcasts/heute-journal_1400x1400.jpg" />
        <itunes:category text="TV &amp; Film"/>
        <item>
            <title>heute journal vom 30.03.2020</title>
            <itunes:author>ZDFde</itunes:author>
            <itunes:subtitle></itunes:subtitle>
            <itunes:summary>Mit den Themen: Deutschland vor der Rezession - Abw&#228;rts dank Corona; Tourismus in der Krise - Leere Betten an der Ostsee; Die Stunde der Populisten - Orban und sein Notstandsgesetz</itunes:summary>
            <link>https://www.zdf.de/nachrichten/heute-journal/heute-journal-vom-30-maerz-2020-100.html</link>
            <enclosure url="http://podfiles.zdf.de/podcast/zdf_podcasts/20/03/200330_sendung_hjo/9/200330_sendung_hjo_1528k_p20v15.mp4" length="297391721" type="video/mp4"/>
            <guid>https://www.zdf.de/uri/0b656f35-cf35-4ba4-9a1c-7fe29576835b</guid>
            <pubDate>Mon, 30 Mar 2020 21:45:00 +0200</pubDate>
            <itunes:duration>1949</itunes:duration>
        </item>
        <item>
            <title>TechStage | Vergleichstest 2020: Die besten smarten Heizkörperthermostate</title>
            <link>https://www.techstage.de/news/UPDATE-Vergleichstest-Sechs-Smart-Home-Heiz-Thermostate-4324482.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
            <description>
                <![CDATA[Smarte Heizkörperthermostate regeln die Temperatur per Heizplan, App und Sprachassistent. Das bringt Komfort und kann Geld sparen. Wir vergleichen zehn Modelle.]]>
            </description>
            <guid isPermaLink="true">https://techstage.de/-4324482</guid>
            <pubDate>Wed, 01 Apr 2020 06:36:00 +0200</pubDate>
            <content:encoded>
                <![CDATA[<p><a href="https://www.techstage.de/news/UPDATE-Vergleichstest-Sechs-Smart-Home-Heiz-Thermostate-4324482.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/4/4/8/vergleichsbild_DB_thermotate-b00bbd6587e8d0c5.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Smarte Heizkörperthermostate regeln die Temperatur per Heizplan, App und Sprachassistent. Das bringt Komfort und kann Geld sparen. Wir vergleichen zehn Modelle.</p>]]>
            </content:encoded>
        </item>
    </channel>
</rss>
"""

let rss1: String =
"""
<?xml version="1.0" encoding="UTF-8"?>
<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">
    <channel>
        <title>heute journal (VIDEO)</title>
        <link>https://www.zdf.de/rss/podcast/video/zdf/nachrichten/heute-journal</link>
        <language>de-de</language>
        <copyright>Copyright Zweites Deutsches Fernsehen, ZDF</copyright>
        <itunes:subtitle></itunes:subtitle>
        <itunes:author>ZDFde</itunes:author>
        <itunes:summary>Politische Berichte, scharfsinnige Analysen, verst&#228;ndliche Erkl&#228;rungen </itunes:summary>
        <itunes:owner>
            <itunes:name>ZDF</itunes:name>
            <itunes:email>webmaster@zdf.de</itunes:email>
        </itunes:owner>
        <itunes:explicit>no</itunes:explicit>
        <itunes:image href="http://module.zdf.de/podcasts/heute-journal_1400x1400.jpg" />
        <itunes:category text="TV &amp; Film"/>
        <item>
            <title>heute journal vom 30.03.2020</title>
            <itunes:author>ZDFde</itunes:author>
            <itunes:subtitle></itunes:subtitle>
            <itunes:summary>Mit den Themen: Deutschland vor der Rezession - Abw&#228;rts dank Corona; Tourismus in der Krise - Leere Betten an der Ostsee; Die Stunde der Populisten - Orban und sein Notstandsgesetz</itunes:summary>
            <link>https://www.zdf.de/nachrichten/heute-journal/heute-journal-vom-30-maerz-2020-100.html</link>
            <enclosure url="http://podfiles.zdf.de/podcast/zdf_podcasts/20/03/200330_sendung_hjo/9/200330_sendung_hjo_1528k_p20v15.mp4" length="297391721" type="video/mp4"/>
            <guid>https://www.zdf.de/uri/0b656f35-cf35-4ba4-9a1c-7fe29576835b</guid>
            <pubDate>Mon, 30 Mar 2020 21:45:00 +0200</pubDate>
            <itunes:duration>1949</itunes:duration>
        </item>
        <item>
            <title>heute journal vom 29.03.2020</title>
            <itunes:author>ZDFde</itunes:author>
            <itunes:subtitle></itunes:subtitle>
            <itunes:summary>Mit den Themen: Kapital f&#252;r die Kleinsten - Berlins 50-Milliarden-Paket; Leben gegen Leben? - Dilemma Corona-Rettung; Alltag in der Praxis: Haus&#228;rzte in Corona-Zeiten</itunes:summary>
            <link>https://www.zdf.de/nachrichten/heute-journal/heute-journal-vom-29-maerz-2020-100.html</link>
            <enclosure url="http://podfiles.zdf.de/podcast/zdf_podcasts/20/03/200329_sendung_hjo/5/200329_sendung_hjo_1528k_p20v15.mp4" length="263327312" type="video/mp4"/>
            <guid>https://www.zdf.de/uri/8bf532c6-929e-4f8f-acfa-1a0cb257cce0</guid>
            <pubDate>Sun, 29 Mar 2020 21:50:00 +0200</pubDate>
            <itunes:duration>1790</itunes:duration>
        </item>
        <item>
            <title>heute journal vom 28.03.2020</title>
            <itunes:author>ZDFde</itunes:author>
            <itunes:subtitle></itunes:subtitle>
            <itunes:summary>Mit den Themen: Aussicht - Ende des Shutdowns nicht vor dem 20. April; Unterst&#252;tzung - Medizin-Studenten packen mit an; Folgen - Corona nach Karneval in New Orleans
</itunes:summary>
            <link>https://www.zdf.de/nachrichten/heute-journal/heute-journal-vom-28-maerz-2020-100.html</link>
            <enclosure url="http://podfiles.zdf.de/podcast/zdf_podcasts/20/03/200328_sendung_hjo/3/200328_sendung_hjo_1528k_p20v15.mp4" length="143465335" type="video/mp4"/>
            <guid>https://www.zdf.de/uri/8825bb59-15f8-4b4e-86c0-03db81f0cc82</guid>
            <pubDate>Sat, 28 Mar 2020 22:45:00 +0100</pubDate>
            <itunes:duration>993</itunes:duration>
        </item>
        <item>
            <title>heute journal vom 27.03.2020</title>
            <itunes:author>ZDFde</itunes:author>
            <itunes:subtitle></itunes:subtitle>
            <itunes:summary>Mit den Themen: Wende von Heinsberg? -  Studie zum Corona-Verlauf; Gef&#228;hrlicher Beistand - Tote Priester in Italien; (K)eine Reform? - Die Rente nach 2025 </itunes:summary>
            <link>https://www.zdf.de/nachrichten/heute-journal/heute-journal-vom-27-03-2020-100.html</link>
            <enclosure url="http://podfiles.zdf.de/podcast/zdf_podcasts/20/03/200327_sendung_hjo/6/200327_sendung_hjo_1528k_p20v15.mp4" length="289226608" type="video/mp4"/>
            <guid>https://www.zdf.de/uri/7e209fa2-9b80-4873-a7b8-370518915186</guid>
            <pubDate>Fri, 27 Mar 2020 22:15:00 +0100</pubDate>
            <itunes:duration>2027</itunes:duration>
        </item>
        <item>
            <title>heute journal vom 26.03.2020</title>
            <itunes:author>ZDFde</itunes:author>
            <itunes:subtitle></itunes:subtitle>
            <itunes:summary>Mit den Themen: Ruhe vor dem Sturm - Kliniken im Krisenmodus; Hilferuf aus New York - &#196;rzte im Kampf gegen Corona; Applaus f&#252;r Helden: Hilfe und Engagement im Alltag</itunes:summary>
            <link>https://www.zdf.de/nachrichten/heute-journal/heute-journal-vom-26-maerz-2020-100.html</link>
            <enclosure url="http://podfiles.zdf.de/podcast/zdf_podcasts/20/03/200326_sendung_hjo/3/200326_sendung_hjo_1528k_p20v15.mp4" length="290467335" type="video/mp4"/>
            <guid>https://www.zdf.de/uri/ff6d93ff-2a24-47aa-8678-d6f2cfd4c9ad</guid>
            <pubDate>Thu, 26 Mar 2020 22:00:00 +0100</pubDate>
            <itunes:duration>1925</itunes:duration>
        </item>
        <item>
            <title>heute journal vom 25.03.2020</title>
            <itunes:author>ZDFde</itunes:author>
            <itunes:subtitle></itunes:subtitle>
            <itunes:summary>Mit den Themen: Milliarden gegen die Corona-Krise - Bundestag beschlie&#223;t Hilfspaket; Was sagen die Corona-Zahlen aus? - Infektiologe Wendtner im Gespr&#228;ch; &#220;berleben in Corona-Zeiten - Wie Kleinh&#228;ndler die Krise bew&#228;ltigen</itunes:summary>
            <link>https://www.zdf.de/nachrichten/heute-journal/heute-journal-vom-25-maerz-2020-100.html</link>
            <enclosure url="http://podfiles.zdf.de/podcast/zdf_podcasts/20/03/200325_sendung_hjo/3/200325_sendung_hjo_1528k_p20v15.mp4" length="264747741" type="video/mp4"/>
            <guid>https://www.zdf.de/uri/6bf63e1d-0b5d-46d4-9e72-0d3c1c358504</guid>
            <pubDate>Wed, 25 Mar 2020 22:00:00 +0100</pubDate>
            <itunes:duration>1760</itunes:duration>
        </item>
        <item>
            <title>heute journal vom 24.03.2020</title>
            <itunes:author>ZDFde</itunes:author>
            <itunes:subtitle></itunes:subtitle>
            <itunes:summary>Mit den Themen: Wuhans Wiederer&#246;ffnung - Das Ende der Quarant&#228;ne?; Corona-Virus-Welle trifft USA - Trump will Verbote lockern; Unb&#252;rokratisch und effizient? - Hilfe f&#252;r Kleinunternehmer</itunes:summary>
            <link>https://www.zdf.de/nachrichten/heute-journal/heute-journal-vom-24-03-2020-102.html</link>
            <enclosure url="http://podfiles.zdf.de/podcast/zdf_podcasts/20/03/200324_sendung_hjo/4/200324_sendung_hjo_1528k_p20v15.mp4" length="276850912" type="video/mp4"/>
            <guid>https://www.zdf.de/uri/e6d1457e-8baa-4fc6-a61c-d121da500aba</guid>
            <pubDate>Tue, 24 Mar 2020 21:50:00 +0100</pubDate>
            <itunes:duration>1892</itunes:duration>
        </item>
    </channel>
</rss>
"""

let rss2: String =
"""
<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/">
    <channel>
        <title>heise online News</title>
        <link>https://www.heise.de/</link>
        <description>Nachrichten nicht nur aus der Welt der Computer</description>
        <lastBuildDate>Wed, 01 Apr 2020 06:36:00 +0200</lastBuildDate>
        <language>de</language>
        <item>
            <title>TechStage | Vergleichstest 2020: Die besten smarten Heizkörperthermostate</title>
            <link>https://www.techstage.de/news/UPDATE-Vergleichstest-Sechs-Smart-Home-Heiz-Thermostate-4324482.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
            <description>
                <![CDATA[Smarte Heizkörperthermostate regeln die Temperatur per Heizplan, App und Sprachassistent. Das bringt Komfort und kann Geld sparen. Wir vergleichen zehn Modelle.]]>
            </description>
            <guid isPermaLink="true">https://techstage.de/-4324482</guid>
            <pubDate>Wed, 01 Apr 2020 06:36:00 +0200</pubDate>
            <content:encoded>
                <![CDATA[<p><a href="https://www.techstage.de/news/UPDATE-Vergleichstest-Sechs-Smart-Home-Heiz-Thermostate-4324482.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/4/4/8/vergleichsbild_DB_thermotate-b00bbd6587e8d0c5.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Smarte Heizkörperthermostate regeln die Temperatur per Heizplan, App und Sprachassistent. Das bringt Komfort und kann Geld sparen. Wir vergleichen zehn Modelle.</p>]]>
    </content:encoded>
</item>
<item>
    <title>New Yorker Staatsanwältin prüft Datenschutz bei Konferenz-App Zoom </title>
    <link>https://www.heise.de/newsticker/meldung/New-Yorker-Staatsanwaeltin-prueft-Datenschutz-bei-Konferenz-App-Zoom-4694352.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Videokonferenzen via Zoom haben an Beliebtheit gewonnen. Die New Yorker Generalstaatsanwaltschaft will den Dienst nun genauer prüfen.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4694352</guid>
    <pubDate>Wed, 01 Apr 2020 06:35:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/New-Yorker-Staatsanwaeltin-prueft-Datenschutz-bei-Konferenz-App-Zoom-4694352.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/7/6/1/shutterstock_1175051545-0b43a922bcec6573.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Videokonferenzen via Zoom haben an Beliebtheit gewonnen. Die New Yorker Generalstaatsanwaltschaft will den Dienst nun genauer prüfen.</p>]]>
</content:encoded>
</item>
<item>
    <title>Blowie: Autonomer Laubbläser mit Laubdetektion und Benzinantrieb</title>
    <link>https://www.heise.de/newsticker/meldung/Blowie-Autonomer-Laubblaeser-mit-Laubdetektion-und-Benzinantrieb-4692368.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Eine US-Firma hat einen autonomen Laubbläser entwickelt, der herbstliche Gehwege reinigen soll. Ein Verbrennungsmotor soll die Laufzeit verlängern.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4692368</guid>
    <pubDate>Wed, 01 Apr 2020 01:04:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Blowie-Autonomer-Laubblaeser-mit-Laubdetektion-und-Benzinantrieb-4692368.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/0/0/1/8/Laubblaeser_1-823c8ea7752250f1.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Eine US-Firma hat einen autonomen Laubbläser entwickelt, der herbstliche Gehwege reinigen soll. Ein Verbrennungsmotor soll die Laufzeit verlängern.</p>]]>
</content:encoded>
</item>
<item>
    <title>TechStage | Sport zu Hause: Smart daheim trainieren</title>
    <link>https://www.techstage.de/news/Sport-zu-Hause-Smart-daheim-trainieren-4158236.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[ Zu viel Zeit, zu wenig zu tun. Wir zeigen Apps und Sportarten, mit denen man mit wenig Aufwand zu Hause ein ordentliches Workout hinbekommt. ]]>
    </description>
    <guid isPermaLink="true">https://techstage.de/-4158236</guid>
    <pubDate>Tue, 31 Mar 2020 20:31:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.techstage.de/news/Sport-zu-Hause-Smart-daheim-trainieren-4158236.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/5/9/8/LaufAppsAndroid_HO-58f28ce0144aa0e5.png" class="webfeedsFeaturedVisual" alt="" /></a></p><p> Zu viel Zeit, zu wenig zu tun. Wir zeigen Apps und Sportarten, mit denen man mit wenig Aufwand zu Hause ein ordentliches Workout hinbekommt. </p>]]>
</content:encoded>
</item>
<item>
    <title>heise+ | Fotowerkstatt: Professionelle Bildbearbeitung mit Affinity Photo</title>
    <link>https://www.heise.de/ratgeber/Fotowerkstatt-Professionelle-Bildbearbeitung-mit-Affinity-Photo-4693506.html?wt_mc=rss.red.ho.ho.rdf.beitrag_plus.beitrag_plus</link>
    <description>
        <![CDATA[Es muss nicht immer Photoshop sein. Wir zeigen, wie Sie typische Bildbearbeitungsfälle mit der günstigeren Alternative Affinity Photo meistern.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693506</guid>
    <pubDate>Tue, 31 Mar 2020 20:00:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/ratgeber/Fotowerkstatt-Professionelle-Bildbearbeitung-mit-Affinity-Photo-4693506.html?wt_mc=rss.red.ho.ho.rdf.beitrag_plus.beitrag_plus"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/02/2/8/7/1/0/1/6/image-1572279351444235-843a0743fe456a71.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Es muss nicht immer Photoshop sein. Wir zeigen, wie Sie typische Bildbearbeitungsfälle mit der günstigeren Alternative Affinity Photo meistern.</p>]]>
</content:encoded>
</item>
<item>
    <title>Erneut Datenleck bei Marriott: Mutmaßlich 5,2 Millionen Hotelgäste betroffen</title>
    <link>https://www.heise.de/newsticker/meldung/Erneut-Datenleck-bei-Marriott-Mutmasslich-5-2-Millionen-Hotelgaeste-betroffen-4694283.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Unbekannte haben auf Daten von vermutlich 5,2 Millionen Gästen der Hotelkette Marriott zugreifen können, wie das Unternehmen eingestand.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4694283</guid>
    <pubDate>Tue, 31 Mar 2020 19:30:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Erneut-Datenleck-bei-Marriott-Mutmasslich-5-2-Millionen-Hotelgaeste-betroffen-4694283.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/6/9/5/shutterstock_70213381-0c4d6cb19d0ce8e5.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Unbekannte haben auf Daten von vermutlich 5,2 Millionen Gästen der Hotelkette Marriott zugreifen können, wie das Unternehmen eingestand.</p>]]>
</content:encoded>
</item>
<item>
    <title>TechStage | Xbox One, PS4 &amp; Switch: Was gibt's fürs Geld?</title>
    <link>https://www.techstage.de/news/Xbox-One-PS4-Switch-Was-gibt-s-fuers-Geld-4694175.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Xbox One, PS4 oder Switch: Selbst Fans behalten kaum noch den Überblick, was Funktionen, Kosten und Einschränkungen angeht. Wir zeigen die Unterschiede. ]]>
    </description>
    <guid isPermaLink="true">https://techstage.de/-4694175</guid>
    <pubDate>Tue, 31 Mar 2020 19:30:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.techstage.de/news/Xbox-One-PS4-Switch-Was-gibt-s-fuers-Geld-4694175.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/6/4/5/Xbox_One_Zubehoer_db-fd8ca6c2d7e92818.png" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Xbox One, PS4 oder Switch: Selbst Fans behalten kaum noch den Überblick, was Funktionen, Kosten und Einschränkungen angeht. Wir zeigen die Unterschiede. </p>]]>
</content:encoded>
</item>
<item>
    <title>Kurz informiert: Buchbinder, VW, Microsoft Cloud, MacOS</title>
    <link>https://www.heise.de/newsticker/meldung/Kurz-informiert-Buchbinder-VW-Microsoft-Cloud-MacOS-4694043.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Unser werktäglicher News-Überblick fasst die wichtigsten Nachrichten des Tages kurz und knapp zusammen.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4694043</guid>
    <pubDate>Tue, 31 Mar 2020 18:55:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Kurz-informiert-Buchbinder-VW-Microsoft-Cloud-MacOS-4694043.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/4/8/3/Einzelbild-f2aa186b0e46103e.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Unser werktäglicher News-Überblick fasst die wichtigsten Nachrichten des Tages kurz und knapp zusammen.</p>]]>
</content:encoded>
</item>
<item>
    <title>Eclipse Theia 1.0 fordert Visual Studio Code heraus</title>
    <link>https://www.heise.de/developer/meldung/Eclipse-Theia-1-0-fordert-Visual-Studio-Code-heraus-4694118.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Die Plattform zum Erstellen Desktop- und Web-basierter IDEs wird als "echte" Open-Source-Alternative zu Microsofts Visual Studio Code ins Spiel gebracht.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4694118</guid>
    <pubDate>Tue, 31 Mar 2020 18:24:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/developer/meldung/Eclipse-Theia-1-0-fordert-Visual-Studio-Code-heraus-4694118.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/5/4/6/malen-5c1912abc59615fc.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Die Plattform zum Erstellen Desktop- und Web-basierter IDEs wird als "echte" Open-Source-Alternative zu Microsofts Visual Studio Code ins Spiel gebracht.</p>]]>
</content:encoded>
</item>
<item>
    <title>Coronakrise: Spielemesse Gamescom soll &quot;zumindest digital stattfinden&quot;</title>
    <link>https://www.heise.de/newsticker/meldung/Coronakrise-Spielemesse-Gamescom-soll-zumindest-digital-stattfinden-4694269.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Im August soll die Gamescom 2020 stattfinden – doch ob das vor Ort in Köln sein kann oder doch nur rein digital, ist angesichts der Corona-Pandemie noch offen.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4694269</guid>
    <pubDate>Tue, 31 Mar 2020 18:19:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Coronakrise-Spielemesse-Gamescom-soll-zumindest-digital-stattfinden-4694269.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/6/8/1/XBox_PS-ac7456d3a5dc6449.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Im August soll die Gamescom 2020 stattfinden – doch ob das vor Ort in Köln sein kann oder doch nur rein digital, ist angesichts der Corona-Pandemie noch offen.</p>]]>
</content:encoded>
</item>
<item>
    <title>Corona-App soll Informationsfluss beschleunigen</title>
    <link>https://www.heise.de/newsticker/meldung/Corona-App-soll-Informationsfluss-beschleunigen-4694251.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Die Deutsche Telekom und die BS Software Development haben eine App entwickelt, über die Laborergebnisse nach einem Abstrich abgefragt werden können.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4694251</guid>
    <pubDate>Tue, 31 Mar 2020 17:55:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Corona-App-soll-Informationsfluss-beschleunigen-4694251.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/6/6/3/COVID-19-App-Bild01-cbde0f519df9e238.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Die Deutsche Telekom und die BS Software Development haben eine App entwickelt, über die Laborergebnisse nach einem Abstrich abgefragt werden können.</p>]]>
</content:encoded>
</item>
<item>
    <title>Deutsches Forschungsnetz und Telekom: Peeren in Zeiten von Corona </title>
    <link>https://www.heise.de/newsticker/meldung/Deutsches-Forschungsnetz-und-Telekom-Peeren-in-Zeiten-von-Corona-4694172.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Nicht nur als Krisenlösung sucht das Deutsche Forschungsnetz den direkten Anschluss zur Deutschen Telekom. Die zeigt aber die kalte (kommerzielle) Schulter.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4694172</guid>
    <pubDate>Tue, 31 Mar 2020 17:02:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Deutsches-Forschungsnetz-und-Telekom-Peeren-in-Zeiten-von-Corona-4694172.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/5/8/4/shutterstock_1391207630-35f0c86fb920d25b.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Nicht nur als Krisenlösung sucht das Deutsche Forschungsnetz den direkten Anschluss zur Deutschen Telekom. Die zeigt aber die kalte (kommerzielle) Schulter.</p>]]>
</content:encoded>
</item>
<item>
    <title>Abzocke im Onlinehandel: Mühsamer Kampf gegen Corona-Geschäfte</title>
    <link>https://www.heise.de/newsticker/meldung/Abzocke-im-Onlinehandel-Muehsamer-Kampf-gegen-Corona-Geschaefte-4694227.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Verbraucherschützer warnen vor Online-Angeboten, die die Corona-Krise ausnutzen wollen. Plattformen wie eBay und Amazon kommen mit dem Löschen kaum hinterher.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4694227</guid>
    <pubDate>Tue, 31 Mar 2020 17:02:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Abzocke-im-Onlinehandel-Muehsamer-Kampf-gegen-Corona-Geschaefte-4694227.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/6/3/9/Online-Shop-c70544a0f19c5d5c.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Verbraucherschützer warnen vor Online-Angeboten, die die Corona-Krise ausnutzen wollen. Plattformen wie eBay und Amazon kommen mit dem Löschen kaum hinterher.</p>]]>
</content:encoded>
</item>
<item>
    <title>heise+ | So berechenbar ist das Coronavirus </title>
    <link>https://www.heise.de/hintergrund/So-berechenbar-ist-das-Coronavirus-4685730.html?wt_mc=rss.red.ho.ho.rdf.beitrag_plus.beitrag_plus</link>
    <description>
        <![CDATA[Mit Computermodellen wollen Forscher ermitteln, wie sich das neue Coronavirus Sars-CoV-2 ausbreitet und wie viele Opfer die Krankheitswelle fordern könnte.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4685730</guid>
    <pubDate>Tue, 31 Mar 2020 17:00:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/hintergrund/So-berechenbar-ist-das-Coronavirus-4685730.html?wt_mc=rss.red.ho.ho.rdf.beitrag_plus.beitrag_plus"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/02/2/8/6/4/2/7/0/fokus_ausbreitung_seuchen_rz-703866ad75d8a57f.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Mit Computermodellen wollen Forscher ermitteln, wie sich das neue Coronavirus Sars-CoV-2 ausbreitet und wie viele Opfer die Krankheitswelle fordern könnte.</p>]]>
</content:encoded>
</item>
<item>
    <title>Zugriffsrechte zurücksetzen: Apple empfiehlt Neuinstallation von macOS</title>
    <link>https://www.heise.de/mac-and-i/meldung/Zugriffsrechte-zuruecksetzen-Apple-empfiehlt-Neuinstallation-von-macOS-4694225.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Das Reparieren der Berechtigungen im Benutzerordner war bisher einfach. Nun sollen Mac-Nutzer gleich das ganze Betriebssystem neu installieren.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4694225</guid>
    <pubDate>Tue, 31 Mar 2020 16:57:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/mac-and-i/meldung/Zugriffsrechte-zuruecksetzen-Apple-empfiehlt-Neuinstallation-von-macOS-4694225.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/6/3/7/Bildschirmfoto_2020-03-31_um_16-29957c263391d401.png" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Das Reparieren der Berechtigungen im Benutzerordner war bisher einfach. Nun sollen Mac-Nutzer gleich das ganze Betriebssystem neu installieren.</p>]]>
</content:encoded>
</item>
<item>
    <title>Huawei wächst trotz Handelskriegs mit den USA zweistellig</title>
    <link>https://www.heise.de/newsticker/meldung/Huawei-waechst-trotz-Handelskriegs-mit-den-USA-zweistellig-4694148.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Auch unter dem politischen Druck aus den USA kann Huawei weiterhin gute Wachstumszahlen präsentieren.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4694148</guid>
    <pubDate>Tue, 31 Mar 2020 16:07:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Huawei-waechst-trotz-Handelskriegs-mit-den-USA-zweistellig-4694148.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/5/6/4/shutterstock_1396636169-f3bb09017f429891.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Auch unter dem politischen Druck aus den USA kann Huawei weiterhin gute Wachstumszahlen präsentieren.</p>]]>
</content:encoded>
</item>
<item>
    <title>Microsoft fixt Windows 10 VPN-Bug mit optionalen Sonderupdates</title>
    <link>https://www.heise.de/newsticker/meldung/Microsoft-fixt-Windows-10-VPN-Bug-mit-optionalen-Sonderupdates-4694177.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Microsoft bringt Windows-10-Updates, die einen Fehler beim Internetzugang beheben sollen, speziell wenn VPN-Software mit Proxy-Konfigurationen verwendet wird.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4694177</guid>
    <pubDate>Tue, 31 Mar 2020 16:01:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Microsoft-fixt-Windows-10-VPN-Bug-mit-optionalen-Sonderupdates-4694177.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/5/8/9/shutterstock_293983433-6d80e7190ebd00be.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Microsoft bringt Windows-10-Updates, die einen Fehler beim Internetzugang beheben sollen, speziell wenn VPN-Software mit Proxy-Konfigurationen verwendet wird.</p>]]>
</content:encoded>
</item>
<item>
    <title>Tabs und Collections: Microsoft Edge bekommt neue Funktionen</title>
    <link>https://www.heise.de/newsticker/meldung/Tabs-und-Collections-Microsoft-Edge-bekommt-neue-Funktionen-4694083.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Microsofts Browser Edge bekommt eine veränderte Tab-Auswahl und neue Collections- sowie Kopierfunktion. ]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4694083</guid>
    <pubDate>Tue, 31 Mar 2020 15:04:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Tabs-und-Collections-Microsoft-Edge-bekommt-neue-Funktionen-4694083.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/5/1/5/Bildschirmfoto_2020-03-31_um_14-b1488cf9d2cec7e1.png" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Microsofts Browser Edge bekommt eine veränderte Tab-Auswahl und neue Collections- sowie Kopierfunktion. </p>]]>
</content:encoded>
</item>
<item>
    <title>Containerisierung: Rancher 2.4 wächst über den Rand hinaus</title>
    <link>https://www.heise.de/developer/meldung/Containerisierung-Rancher-2-4-waechst-ueber-den-Rand-hinaus-4693261.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Die Verwaltungsplattform der auf begrenzte Ressourcen ausgelegten Kubernetes-Distributionen K3s skaliert im Edge-Computing nun bis zu einer Million Cluster.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693261</guid>
    <pubDate>Tue, 31 Mar 2020 15:00:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/developer/meldung/Containerisierung-Rancher-2-4-waechst-ueber-den-Rand-hinaus-4693261.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/0/8/0/0/Rancher_2_2-ef22e7bae2e10c78.png" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Die Verwaltungsplattform der auf begrenzte Ressourcen ausgelegten Kubernetes-Distributionen K3s skaliert im Edge-Computing nun bis zu einer Million Cluster.</p>]]>
</content:encoded>
</item>
<item>
    <title>MariaDB mit kombinierter Datenbank-SaaS</title>
    <link>https://www.heise.de/newsticker/meldung/MariaDB-mit-kombinierter-Datenbank-SaaS-4693117.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[MariaDB bringt unter dem Namen SkySQL eine cloudgestützte Datenbank heraus, die gleichzeitig Transaktionen und analytische Anfragen abarbeiten soll.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693117</guid>
    <pubDate>Tue, 31 Mar 2020 14:58:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/MariaDB-mit-kombinierter-Datenbank-SaaS-4693117.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/0/7/0/3/SkySQL-Aufmacher-c1c57e450f123cc1.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>MariaDB bringt unter dem Namen SkySQL eine cloudgestützte Datenbank heraus, die gleichzeitig Transaktionen und analytische Anfragen abarbeiten soll.</p>]]>
</content:encoded>
</item>
<item>
    <title>Anne Frank im Video-Tagebuch auf Youtube</title>
    <link>https://www.heise.de/newsticker/meldung/Anne-Frank-im-Video-Tagebuch-auf-Youtube-4694087.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Anne Franks Tagebuch als Videoserie: Die Anne-Frank-Stiftung will jungen Zuschauern mit ungewöhnlichen Mitteln das Schicksal des Mädchens nahebringen.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4694087</guid>
    <pubDate>Tue, 31 Mar 2020 14:50:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Anne-Frank-im-Video-Tagebuch-auf-Youtube-4694087.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/5/1/9/Anne_Frank_video_diary__Luna__Anne__with_camera__copyright_Anne_Frank_House__fotographer_Ray_van_der_Bas-4e09a2608a872a40.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Anne Franks Tagebuch als Videoserie: Die Anne-Frank-Stiftung will jungen Zuschauern mit ungewöhnlichen Mitteln das Schicksal des Mädchens nahebringen.</p>]]>
</content:encoded>
</item>
<item>
    <title>Steam verteilt automatische Spiele-Updates, um Netz zu schonen</title>
    <link>https://www.heise.de/newsticker/meldung/Steam-verteilt-automatische-Spiele-Updates-um-Netz-zu-schonen-4694070.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Gelegenheitsspieler müssen Patch-Downloads über Steam gegebenenfalls öfter händisch anstupsen, bevor sie loslegen können.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4694070</guid>
    <pubDate>Tue, 31 Mar 2020 14:48:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Steam-verteilt-automatische-Spiele-Updates-um-Netz-zu-schonen-4694070.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/5/0/2/shutterstock_703264159-dbfb7025ce784a34.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Gelegenheitsspieler müssen Patch-Downloads über Steam gegebenenfalls öfter händisch anstupsen, bevor sie loslegen können.</p>]]>
</content:encoded>
</item>
<item>
    <title>Internet Society: Internet-Schwergewichte wollen das Routing stärker absichern</title>
    <link>https://www.heise.de/newsticker/meldung/ISOC-Internet-Schwergewichte-wollen-das-Routing-staerker-absichern-4693910.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Betreiber von Serverfarmen wie Akamai, Amazon, Cloudflare, Facebook oder Netflix haben sich der MANRS-Initiative für sichereres Routing angeschlossen.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693910</guid>
    <pubDate>Tue, 31 Mar 2020 14:00:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/ISOC-Internet-Schwergewichte-wollen-das-Routing-staerker-absichern-4693910.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/3/6/3/__xfffd_berwachung-f865ec5e4b20278f.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Betreiber von Serverfarmen wie Akamai, Amazon, Cloudflare, Facebook oder Netflix haben sich der MANRS-Initiative für sichereres Routing angeschlossen.</p>]]>
</content:encoded>
</item>
<item>
    <title>heise+ | Ableton Live: Performen wie die Profis</title>
    <link>https://www.heise.de/ratgeber/Ableton-Live-Performen-wie-die-Profis-4691885.html?wt_mc=rss.red.ho.ho.rdf.beitrag_plus.beitrag_plus</link>
    <description>
        <![CDATA[Drauflos jammen, Sounds und Beats mixen und eigene musikalische Ideen umsetzen? Wir zeigen Ihnen, wie das mit Live von Ableton gelingt – auch auf der Bühne.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4691885</guid>
    <pubDate>Tue, 31 Mar 2020 14:00:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/ratgeber/Ableton-Live-Performen-wie-die-Profis-4691885.html?wt_mc=rss.red.ho.ho.rdf.beitrag_plus.beitrag_plus"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/02/2/8/6/9/6/3/0/image-1572267800122661-46bdd150c4ccca89.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Drauflos jammen, Sounds und Beats mixen und eigene musikalische Ideen umsetzen? Wir zeigen Ihnen, wie das mit Live von Ableton gelingt – auch auf der Bühne.</p>]]>
</content:encoded>
</item>
<item>
    <title>Zahlen, bitte! Amerikas erste 111 Tage Im Satellitenzeitalter</title>
    <link>https://www.heise.de/newsticker/meldung/Zahlen-bitte-Amerikas-erste-111-Tage-Im-Satellitenzeitalter-4693644.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Der Satellit Explorer 1 war nicht nur der US-amerikanische Start ins Satellitenzeitalter - er war auch die erfolgreiche Antwort auf den russischen Sputnik.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693644</guid>
    <pubDate>Tue, 31 Mar 2020 13:37:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Zahlen-bitte-Amerikas-erste-111-Tage-Im-Satellitenzeitalter-4693644.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/1/5/4/ZahlenBitte-566bed769a2cf058.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Der Satellit Explorer 1 war nicht nur der US-amerikanische Start ins Satellitenzeitalter - er war auch die erfolgreiche Antwort auf den russischen Sputnik.</p>]]>
</content:encoded>
</item>
<item>
    <title>Webbrowser Edge will vor geleakten Log-in-Daten warnen</title>
    <link>https://www.heise.de/security/meldung/Webbrowser-Edge-will-vor-geleakten-Log-in-Daten-warnen-4693991.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Microsoft verspricht zeitnah eine neue Funktion für den Gebrauch von sicheren Passwörtern in Edge zu aktivieren. ]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693991</guid>
    <pubDate>Tue, 31 Mar 2020 13:09:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/security/meldung/Webbrowser-Edge-will-vor-geleakten-Log-in-Daten-warnen-4693991.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/4/3/5/urn-newsml-dpa-com-20090101-140403-99-06266_large_4_3-a09d9daacc886bf9.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Microsoft verspricht zeitnah eine neue Funktion für den Gebrauch von sicheren Passwörtern in Edge zu aktivieren. </p>]]>
</content:encoded>
</item>
<item>
    <title>&quot;Derzeit einer der sichersten Orte&quot;: Während der Corona-Krise im Weltall</title>
    <link>https://www.heise.de/newsticker/meldung/Derzeit-einer-der-sichersten-Orte-Waehrend-der-Corona-Krise-im-Weltall-4694034.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Auf der Erde hat die Corona-Krise das Leben komplett verändert – und im Weltraum? Die ISS ist derzeit einer der wohl am sichersten Corona-freien Orte.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4694034</guid>
    <pubDate>Tue, 31 Mar 2020 13:01:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Derzeit-einer-der-sichersten-Orte-Waehrend-der-Corona-Krise-im-Weltall-4694034.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/4/7/4/STS-133_International_Space_Station_after_undocking_5-5eb31d4d151e4864.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Auf der Erde hat die Corona-Krise das Leben komplett verändert – und im Weltraum? Die ISS ist derzeit einer der wohl am sichersten Corona-freien Orte.</p>]]>
</content:encoded>
</item>
<item>
    <title>XMG Apex 15: Notebook mit Desktop-16-Kerner Ryzen 9 3950X</title>
    <link>https://www.heise.de/newsticker/meldung/XMG-Apex-15-Notebook-mit-Desktop-16-Kerner-Ryzen-9-3950X-4693774.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Schenker wechselt bei seinen Desktop-Replacements von Intel auf AMD, um die Performance zu steigern und Lieferengpässe zu vermeiden.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693774</guid>
    <pubDate>Tue, 31 Mar 2020 13:00:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/XMG-Apex-15-Notebook-mit-Desktop-16-Kerner-Ryzen-9-3950X-4693774.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/2/7/3/XMG-APEX-15-E20-17-c655727b4d45fa16.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Schenker wechselt bei seinen Desktop-Replacements von Intel auf AMD, um die Performance zu steigern und Lieferengpässe zu vermeiden.</p>]]>
</content:encoded>
</item>
<item>
    <title>heise-Angebot: IIoT Conference: Das Programm für Dezember ist online</title>
    <link>https://www.heise.de/developer/meldung/IIoT-Conference-Das-Programm-fuer-Dezember-ist-online-4693983.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Zusammen mit der automatica wechselt die IIoT Conference in den Dezember. Das Programm bietet 14 Vorträge in zwei Tracks sowie drei zusätzliche Workshops.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693983</guid>
    <pubDate>Tue, 31 Mar 2020 12:47:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/developer/meldung/IIoT-Conference-Das-Programm-fuer-Dezember-ist-online-4693983.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/4/2/7/iiotconf-f0e37ccda358ae58.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Zusammen mit der automatica wechselt die IIoT Conference in den Dezember. Das Programm bietet 14 Vorträge in zwei Tracks sowie drei zusätzliche Workshops.</p>]]>
</content:encoded>
</item>
<item>
    <title>Intel Comet Lake-H: Notebook-Prozessoren mit hohem Turbo</title>
    <link>https://www.heise.de/newsticker/meldung/Intel-Comet-Lake-H-Notebook-Prozessoren-mit-hohem-Turbo-4693948.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Acht CPU-Kerne und einen Turbo-Takt von bis zu 5,3 GHz auf einzelnen Kernen sollen Intels kommende Mobilprozessoren gegen AMDs Ryzen 4000 auffahren.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693948</guid>
    <pubDate>Tue, 31 Mar 2020 12:37:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Intel-Comet-Lake-H-Notebook-Prozessoren-mit-hohem-Turbo-4693948.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/4/0/1/shutterstock_1159844176-e106a015e9263022.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Acht CPU-Kerne und einen Turbo-Takt von bis zu 5,3 GHz auf einzelnen Kernen sollen Intels kommende Mobilprozessoren gegen AMDs Ryzen 4000 auffahren.</p>]]>
</content:encoded>
</item>
<item>
    <title>Coronavirus-Stillstand: Lockdown-Tipps für den dritten Dienstag</title>
    <link>https://www.heise.de/newsticker/meldung/Coronavirus-Stillstand-Lockdown-Tipps-fuer-den-dritten-Dienstag-4693714.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Wir haben wieder Infos und Tipps zum Zeitvertreib – inklusive eines Bullshit-Bingos. Wer ruft als erstes?]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693714</guid>
    <pubDate>Tue, 31 Mar 2020 12:33:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Coronavirus-Stillstand-Lockdown-Tipps-fuer-den-dritten-Dienstag-4693714.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/2/1/5/IMG_4230-ff88110a56fab381.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Wir haben wieder Infos und Tipps zum Zeitvertreib – inklusive eines Bullshit-Bingos. Wer ruft als erstes?</p>]]>
</content:encoded>
</item>
<item>
    <title>Samsung Display stellt LCD-Produktion Ende 2020 ein</title>
    <link>https://www.heise.de/newsticker/meldung/Samsung-Display-stellt-LCD-Produktion-Ende-2020-ein-4693865.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Samsung Display will keine LCDs mehr produzieren und setzt auf Panels mit Quantum-Dot-Technik.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693865</guid>
    <pubDate>Tue, 31 Mar 2020 11:55:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Samsung-Display-stellt-LCD-Produktion-Ende-2020-ein-4693865.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/3/2/6/shutterstock_1404732647-e3121f741a7860b1.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Samsung Display will keine LCDs mehr produzieren und setzt auf Panels mit Quantum-Dot-Technik.</p>]]>
</content:encoded>
</item>
<item>
    <title>Älterer DSL-Modem-Router von D-Link vielfältig angreifbar</title>
    <link>https://www.heise.de/security/meldung/Aelterer-DSL-Modem-Router-von-D-Link-vielfaeltig-angreifbar-4693812.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Der DSL-Router DSL-2640B ist über mehrere Schwachstellen attackierbar. Sicherheitsforschern zufolge wird es wohl keine Updates geben. ]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693812</guid>
    <pubDate>Tue, 31 Mar 2020 11:37:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/security/meldung/Aelterer-DSL-Modem-Router-von-D-Link-vielfaeltig-angreifbar-4693812.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/2/9/2/shutterstock_75493267-d8b282173ca011ef.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Der DSL-Router DSL-2640B ist über mehrere Schwachstellen attackierbar. Sicherheitsforschern zufolge wird es wohl keine Updates geben. </p>]]>
</content:encoded>
</item>
<item>
    <title>OpenTelemetry: Erstes Beta-Release unterstützt Erlang, Go, Java, JavaScript und Python</title>
    <link>https://www.heise.de/developer/meldung/OpenTelemetry-Erstes-Beta-Release-unterstuetzt-Erlang-Go-Java-JS-und-Python-4693768.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Das Open-Source-Ökosystem für Telemetrie bringt im ersten offiziellen Beta-Release APIs und SDKs für zahlreiche Programmiersprachen.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693768</guid>
    <pubDate>Tue, 31 Mar 2020 11:34:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/developer/meldung/OpenTelemetry-Erstes-Beta-Release-unterstuetzt-Erlang-Go-Java-JS-und-Python-4693768.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/2/6/7/shutterstock_763186900-4f3e82310ee9c0f7.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Das Open-Source-Ökosystem für Telemetrie bringt im ersten offiziellen Beta-Release APIs und SDKs für zahlreiche Programmiersprachen.</p>]]>
</content:encoded>
</item>
<item>
    <title>COVID-19: Smarte Fieberthermometer zeigen US-Ausbreitung fast live</title>
    <link>https://www.heise.de/newsticker/meldung/COVID-19-Smarte-Fieberthermometer-zeigen-US-Ausbreitung-womoeglich-fast-live-4693825.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Ein wichtiges Symptom von COVID-19 ist Fieber; ein US-Unternehmen weiß, wo wieviele seiner Kunden erhöhte Temperaturen haben. Eine Karte gibt einen Einblick.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693825</guid>
    <pubDate>Tue, 31 Mar 2020 11:31:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/COVID-19-Smarte-Fieberthermometer-zeigen-US-Ausbreitung-womoeglich-fast-live-4693825.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/3/0/5/shutterstock_1629206608-ba7d8bf7913ae7c4.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Ein wichtiges Symptom von COVID-19 ist Fieber; ein US-Unternehmen weiß, wo wieviele seiner Kunden erhöhte Temperaturen haben. Eine Karte gibt einen Einblick.</p>]]>
</content:encoded>
</item>
<item>
    <title>Anzeige: IT-Sicherheit: Darum lohnt sich der Wechsel von Office 365 auf Microsoft 365</title>
    <link>https://www.heise.de/brandworlds/cloud-services/it-sicherheit-wieso-sich-ein-wechsel-von-office-365-auf-microsoft-365-lohnt/?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Für den modernen Arbeitsplatz sind neue Sicherheitskonzepte gefragt. In diesem Kontext kann sich ein Upgrade von Office 365 auf Microsoft 365 auszahlen.]]>
    </description>
    <guid isPermaLink="false">http://heise.de/-4693886</guid>
    <pubDate>Tue, 31 Mar 2020 11:08:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/brandworlds/cloud-services/it-sicherheit-wieso-sich-ein-wechsel-von-office-365-auf-microsoft-365-lohnt/?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/3/8/3/Telekom-Brandworld-Teaserbild__24__31_03_20_-_380x214-998989557f40ab9b.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Für den modernen Arbeitsplatz sind neue Sicherheitskonzepte gefragt. In diesem Kontext kann sich ein Upgrade von Office 365 auf Microsoft 365 auszahlen.</p>]]>
</content:encoded>
</item>
<item>
    <title>heise+ | Fix und fertig vom Dienstleister: Das taugen gerahmte Fotodrucke</title>
    <link>https://www.heise.de/ratgeber/Fix-und-fertig-vom-Dienstleister-Das-taugen-gerahmte-Fotodrucke-4690845.html?wt_mc=rss.red.ho.ho.rdf.beitrag_plus.beitrag_plus</link>
    <description>
        <![CDATA[Geschenke kaufen, ohne das Haus zu verlassen: Welche Qualität Sie bei gerahmten Fotodrucken erwarten können, haben wir bei sechs Dienstleistern überprüft.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4690845</guid>
    <pubDate>Tue, 31 Mar 2020 11:00:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/ratgeber/Fix-und-fertig-vom-Dienstleister-Das-taugen-gerahmte-Fotodrucke-4690845.html?wt_mc=rss.red.ho.ho.rdf.beitrag_plus.beitrag_plus"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/02/2/8/6/8/7/3/5/001_Aufmacher-4ed910a39290b53f.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Geschenke kaufen, ohne das Haus zu verlassen: Welche Qualität Sie bei gerahmten Fotodrucken erwarten können, haben wir bei sechs Dienstleistern überprüft.</p>]]>
</content:encoded>
</item>
<item>
    <title>Huawei stellt mit MindSpore ein eigenes Machine-Learning-Framework vor</title>
    <link>https://www.heise.de/developer/meldung/Huawei-stellt-mit-MindSpore-ein-eigenes-Machine-Learning-Framework-vor-4693710.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Ursprünglich im Zusammenhang mit dem Spezialprozessor Ascend 910 angekündigt, betritt das Framework MindSpore nun neun Monate später die Open-Source-Bühne.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693710</guid>
    <pubDate>Tue, 31 Mar 2020 10:42:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/developer/meldung/Huawei-stellt-mit-MindSpore-ein-eigenes-Machine-Learning-Framework-vor-4693710.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/2/1/1/Machine_Learning_1400px-2b17e22225d84877.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Ursprünglich im Zusammenhang mit dem Spezialprozessor Ascend 910 angekündigt, betritt das Framework MindSpore nun neun Monate später die Open-Source-Bühne.</p>]]>
</content:encoded>
</item>
<item>
    <title>heise-Angebot: heise-Workshop: Social Selling und Social Media im B2B</title>
    <link>https://www.heise.de/newsticker/meldung/heise-Workshop-Social-Selling-und-Social-Media-im-B2B-4693379.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Fleißige Posten, Liken und Kommentieren führt nicht immer zum Verkaufserfolg. Dieser Workshop erläutert, worauf bei B2B-Geschäften im Netz zu achten ist.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693379</guid>
    <pubDate>Tue, 31 Mar 2020 10:21:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/heise-Workshop-Social-Selling-und-Social-Media-im-B2B-4693379.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/0/8/9/5/Homeoffice-49b824680be22eb3.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Fleißige Posten, Liken und Kommentieren führt nicht immer zum Verkaufserfolg. Dieser Workshop erläutert, worauf bei B2B-Geschäften im Netz zu achten ist.</p>]]>
</content:encoded>
</item>
<item>
    <title>Java-Blockchain-Projekt: Apache Tuweni springt auf Version 1.0</title>
    <link>https://www.heise.de/developer/meldung/Java-Blockchain-Projekt-Apache-Tuweni-springt-auf-Version-1-0-4693750.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Am Status eines Incubator-Projekts der Apache Software Foundation ändert sich durch die bedeutungsschwangere Freigabe der Version 1.0 aber nichts.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693750</guid>
    <pubDate>Tue, 31 Mar 2020 10:09:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/developer/meldung/Java-Blockchain-Projekt-Apache-Tuweni-springt-auf-Version-1-0-4693750.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/2/4/9/shutterstock_1395082622-4d711b6819b0ab31.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Am Status eines Incubator-Projekts der Apache Software Foundation ändert sich durch die bedeutungsschwangere Freigabe der Version 1.0 aber nichts.</p>]]>
</content:encoded>
</item>
<item>
    <title>Mac-Firmware: Nicht immer auf dem aktuellen Stand – trotz Update</title>
    <link>https://www.heise.de/mac-and-i/meldung/Mac-Firmware-Nicht-immer-auf-dem-aktuellen-Stand-trotz-Update-4693573.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Apples Aktualisierungen sollten sicherstellen, dass auch das EFI auf den neuesten Stand gebracht wird. Aus unerfindlichen Gründen passiert das aber nicht immer.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693573</guid>
    <pubDate>Tue, 31 Mar 2020 10:05:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/mac-and-i/meldung/Mac-Firmware-Nicht-immer-auf-dem-aktuellen-Stand-trotz-Update-4693573.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/0/8/3/mbp16-a16eaa02365f5a4f.png" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Apples Aktualisierungen sollten sicherstellen, dass auch das EFI auf den neuesten Stand gebracht wird. Aus unerfindlichen Gründen passiert das aber nicht immer.</p>]]>
</content:encoded>
</item>
<item>
    <title>Diesel-Skandal: 200.000 Anspruchsberechtigte melden sich für VW-Vergleich</title>
    <link>https://www.heise.de/newsticker/meldung/Diesel-Skandal-200-000-Anspruchsberechtigte-melden-sich-fuer-VW-Vergleich-4693718.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Von den insgesamt rund 260.000 Anspruchsberechtigten für eine Entschädigung im Diesel-Skandal haben sich bisher 200.000 registrieren lassen.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693718</guid>
    <pubDate>Tue, 31 Mar 2020 09:33:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Diesel-Skandal-200-000-Anspruchsberechtigte-melden-sich-fuer-VW-Vergleich-4693718.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/2/1/9/vw-16b61da10470b616.gif" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Von den insgesamt rund 260.000 Anspruchsberechtigten für eine Entschädigung im Diesel-Skandal haben sich bisher 200.000 registrieren lassen.</p>]]>
</content:encoded>
</item>
<item>
    <title>Bundesjustizministerin: Handydaten-Erfassung nur auf freiwilliger Basis</title>
    <link>https://www.heise.de/newsticker/meldung/Bundesjustizministerin-Handydaten-Erfassung-nur-auf-freiwilliger-Basis-4693738.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Um die Coronavirus-Pandemie unter Kontrolle zu bringen, könnten Handy-Apps zum Einsatz kommen – allerdings nur freiwillig, erklärt Christine Lambrecht.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693738</guid>
    <pubDate>Tue, 31 Mar 2020 09:31:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Bundesjustizministerin-Handydaten-Erfassung-nur-auf-freiwilliger-Basis-4693738.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/2/3/9/shutterstock_461082331-c58214bff85d27af.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Um die Coronavirus-Pandemie unter Kontrolle zu bringen, könnten Handy-Apps zum Einsatz kommen – allerdings nur freiwillig, erklärt Christine Lambrecht.</p>]]>
</content:encoded>
</item>
<item>
    <title>Jetzt bewerten: Wettbewerb &quot;Gut zubereitet&quot;</title>
    <link>https://www.heise.de/foto/meldung/Jetzt-bewerten-Wettbewerb-Gut-zubereitet-4693527.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Unser aktueller Fotowettbewerb geht in die Bewertungsphase. Wählen Sie Ihren Favoriten. ]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693527</guid>
    <pubDate>Tue, 31 Mar 2020 09:30:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/foto/meldung/Jetzt-bewerten-Wettbewerb-Gut-zubereitet-4693527.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/0/3/7/suppe-77c4f5acacb20d87.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Unser aktueller Fotowettbewerb geht in die Bewertungsphase. Wählen Sie Ihren Favoriten. </p>]]>
</content:encoded>
</item>
<item>
    <title>Nachfrage nach Microsofts Cloud-Diensten explodiert</title>
    <link>https://www.heise.de/newsticker/meldung/Nachfrage-nach-Microsofts-Cloud-Diensten-explodiert-4693697.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Der Ansturm auf Cloud-Dienste von Microsoft ist derzeit hoch. Microsoft versucht, den Betrieb möglichst reibungslos aufrechtzuerhalten.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693697</guid>
    <pubDate>Tue, 31 Mar 2020 09:24:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Nachfrage-nach-Microsofts-Cloud-Diensten-explodiert-4693697.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/1/9/8/shutterstock_1107716192-b1a5a7ef62d19396.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Der Ansturm auf Cloud-Dienste von Microsoft ist derzeit hoch. Microsoft versucht, den Betrieb möglichst reibungslos aufrechtzuerhalten.</p>]]>
</content:encoded>
</item>
<item>
    <title>GitLab erweitert die Open-Source-Funktionen</title>
    <link>https://www.heise.de/developer/meldung/GitLab-erweitert-die-Open-Source-Funktionen-4693703.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Insgesamt 18 Features landen in der kostenfreien Core-Variante der Versionsverwaltung, und die Betreiber rufen die Community zur Mitarbeit auf.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693703</guid>
    <pubDate>Tue, 31 Mar 2020 09:19:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/developer/meldung/GitLab-erweitert-die-Open-Source-Funktionen-4693703.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/2/0/4/team-046a62bf9a27d1f0.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Insgesamt 18 Features landen in der kostenfreien Core-Variante der Versionsverwaltung, und die Betreiber rufen die Community zur Mitarbeit auf.</p>]]>
</content:encoded>
</item>
<item>
    <title>Programmieren für Kinder: Python und Scratch spielerisch lernen über Ostern</title>
    <link>https://www.heise.de/developer/meldung/Programmieren-fuer-Kinder-Python-und-Scratch-spielerisch-lernen-ueber-Ostern-4693695.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[RoboManiac bietet vom 6. bis 10. April 2020 Online-Feriencamps für Kinder an, die  die Programmiersprachen Python und Scratch spielerisch lernen wollen.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693695</guid>
    <pubDate>Tue, 31 Mar 2020 09:07:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/developer/meldung/Programmieren-fuer-Kinder-Python-und-Scratch-spielerisch-lernen-ueber-Ostern-4693695.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/1/9/6/shutterstock_1075168772-417653c25a5e0ee1.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>RoboManiac bietet vom 6. bis 10. April 2020 Online-Feriencamps für Kinder an, die  die Programmiersprachen Python und Scratch spielerisch lernen wollen.</p>]]>
</content:encoded>
</item>
<item>
    <title>SoftwareArchitekTOUR, Episode 70: Remote Work in Corona-Zeiten</title>
    <link>https://www.heise.de/developer/artikel/Episode-70-Remote-Work-in-Corona-Zeiten-4692875.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Die Sonderepisode Podcasts auf heise Developer beleuchtet die Erfahrungen erfahrener ITler mit der Arbeit im Homeoffice. ]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4692875</guid>
    <pubDate>Tue, 31 Mar 2020 08:45:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/developer/artikel/Episode-70-Remote-Work-in-Corona-Zeiten-4692875.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/0/5/8/microphone-338481_1920-e3dfa91ba06585a6.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Die Sonderepisode Podcasts auf heise Developer beleuchtet die Erfahrungen erfahrener ITler mit der Arbeit im Homeoffice. </p>]]>
</content:encoded>
</item>
<item>
    <title>heise-Angebot: heise Security: Drei Webinar-Pakete für Security- und Datenschutz-Profis</title>
    <link>https://www.heise.de/security/meldung/heise-Security-Drei-Webinar-Pakete-fuer-Security-und-Datenschutz-Profis-4693319.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Mit heise-Security-Webinaren eignen Sie sich praxisrelevantes Wissen für IT-Security oder Datenschutz an. Jetzt gibt es die in besonders günstigen Angeboten.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693319</guid>
    <pubDate>Tue, 31 Mar 2020 08:30:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/security/meldung/heise-Security-Drei-Webinar-Pakete-fuer-Security-und-Datenschutz-Profis-4693319.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/0/8/4/5/Webinarpaket_1280x720-7dc552dcf88b1741.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Mit heise-Security-Webinaren eignen Sie sich praxisrelevantes Wissen für IT-Security oder Datenschutz an. Jetzt gibt es die in besonders günstigen Angeboten.</p>]]>
</content:encoded>
</item>
<item>
    <title>heise+ | Umweltsensoren für eigene Messstationen zu Luftqualität, Feinstaub, Lärm</title>
    <link>https://www.heise.de/ratgeber/Umweltsensoren-fuer-eigene-Messstationen-zu-Luftqualitaet-und-mehr-4684847.html?wt_mc=rss.red.ho.ho.rdf.beitrag_plus.beitrag_plus</link>
    <description>
        <![CDATA[Ob Feinstaub oder Lärm – die Luft- und Umweltqualität treibt viele um. Bausätze vereinfachen den Bau eigener Messstationen. Doch welcher eignet sich für wen?]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4684847</guid>
    <pubDate>Tue, 31 Mar 2020 08:00:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/ratgeber/Umweltsensoren-fuer-eigene-Messstationen-zu-Luftqualitaet-und-mehr-4684847.html?wt_mc=rss.red.ho.ho.rdf.beitrag_plus.beitrag_plus"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/02/2/8/6/3/6/2/5/image-158159635513908-f72d91a0c5ac0772.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Ob Feinstaub oder Lärm – die Luft- und Umweltqualität treibt viele um. Bausätze vereinfachen den Bau eigener Messstationen. Doch welcher eignet sich für wen?</p>]]>
</content:encoded>
</item>
<item>
    <title>Virtuelle Spurenerfassung für Polizisten</title>
    <link>https://www.heise.de/newsticker/meldung/Portable-Spurenerfassung-fuer-Polizisten-4668902.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Mit dem 3DF-Scanner Jenaer Wissenschaftler lassen sich Tatorte dreidimensional erfassen.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4668902</guid>
    <pubDate>Tue, 31 Mar 2020 07:44:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Portable-Spurenerfassung-fuer-Polizisten-4668902.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/5/0/0/9/5/spuren3-4c8cab6b72f2de87.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Mit dem 3DF-Scanner Jenaer Wissenschaftler lassen sich Tatorte dreidimensional erfassen.</p>]]>
</content:encoded>
</item>
<item>
    <title>Gesetz gegen Hassnachrichten: Justizministerin Lambrecht verteidigt Entwurf</title>
    <link>https://www.heise.de/newsticker/meldung/Gesetz-gegen-Hassnachrichten-Justizministerin-Lambrecht-verteidigt-Entwurf-4693676.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Die Justizministerin will Lücken in der Gesetzgebung schließen, die von Ermittlern benannt wurden. Es gehe um schlimmste Drohungen mit Straftaten.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693676</guid>
    <pubDate>Tue, 31 Mar 2020 07:43:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Gesetz-gegen-Hassnachrichten-Justizministerin-Lambrecht-verteidigt-Entwurf-4693676.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/1/8/0/shutterstock_1354181054-0402b793b20c71e2.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Die Justizministerin will Lücken in der Gesetzgebung schließen, die von Ermittlern benannt wurden. Es gehe um schlimmste Drohungen mit Straftaten.</p>]]>
</content:encoded>
</item>
<item>
    <title>Office 365 heißt jetzt Microsoft 365</title>
    <link>https://www.heise.de/news/Office-365-heisst-jetzt-Microsoft-365-4693611.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Microsoft ersetzt die Abo-Modelle Office 365 Personal und Home durch Microsoft 365 Single und Family. Mit an Bord: eine spezielle Version von Teams.
]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693611</guid>
    <pubDate>Tue, 31 Mar 2020 07:40:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/news/Office-365-heisst-jetzt-Microsoft-365-4693611.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/1/2/1/MS365-257dce8a94ea7b7d.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Microsoft ersetzt die Abo-Modelle Office 365 Personal und Home durch Microsoft 365 Single und Family. Mit an Bord: eine spezielle Version von Teams.
</p>]]>
</content:encoded>
</item>
<item>
    <title>PartChain: BMW will Lieferketten mit Blockchain-Technik nachvollziehen</title>
    <link>https://www.heise.de/newsticker/meldung/PartChain-BMW-will-Lieferketten-mit-Blockchain-Technik-nachvollziehen-4693682.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Mit PartChain sollen Lieferketten von BMW künftig transparent nachverfolgbar und manipulationssicher sein. ]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693682</guid>
    <pubDate>Tue, 31 Mar 2020 07:26:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/PartChain-BMW-will-Lieferketten-mit-Blockchain-Technik-nachvollziehen-4693682.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/1/8/4/shutterstock_749922058-31ab48a0357923a3.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Mit PartChain sollen Lieferketten von BMW künftig transparent nachverfolgbar und manipulationssicher sein. </p>]]>
</content:encoded>
</item>
<item>
    <title>Der Autovermieter Buchbinder lässt Kunden über sein Datenleck im Unklaren </title>
    <link>https://www.heise.de/ct/artikel/Der-Autovermieter-Buchbinder-laesst-Kunden-ueber-sein-Datenleck-im-Unklaren-4688697.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Die Autovermietung Buchbinder laviert sich schweigsam durch die Folgen ihrer riesigen Datenpanne, die c’t und Die Zeit im Januar aufdeckten. ]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4688697</guid>
    <pubDate>Tue, 31 Mar 2020 07:00:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/ct/artikel/Der-Autovermieter-Buchbinder-laesst-Kunden-ueber-sein-Datenleck-im-Unklaren-4688697.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/1/3/3/Buchbinder_Titel-44e5d9aa73d44a1b.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Die Autovermietung Buchbinder laviert sich schweigsam durch die Folgen ihrer riesigen Datenpanne, die c’t und Die Zeit im Januar aufdeckten. </p>]]>
</content:encoded>
</item>
<item>
    <title>heise+ | Spezialtechnik in der Naturfotografie: Von der Bodendrohne bis zur Kamerafalle</title>
    <link>https://www.heise.de/ratgeber/Spezialtechnik-in-der-Naturfotografie-Von-der-Bodendrohne-bis-zur-Kamerafallen-4677632.html?wt_mc=rss.red.ho.ho.rdf.beitrag_plus.beitrag_plus</link>
    <description>
        <![CDATA[Wildlife-Fotograf Will Burrard-Lucas ist berühmt für seine außergewöhnlichen Weitwinkel- und Nachtaufnahmen. Uns zeigt der junge Brite seine Tricks.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4677632</guid>
    <pubDate>Mon, 30 Mar 2020 20:00:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/ratgeber/Spezialtechnik-in-der-Naturfotografie-Von-der-Bodendrohne-bis-zur-Kamerafallen-4677632.html?wt_mc=rss.red.ho.ho.rdf.beitrag_plus.beitrag_plus"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/02/2/8/5/7/5/3/9/001_Aufmacher-6802a0352ee010ab.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Wildlife-Fotograf Will Burrard-Lucas ist berühmt für seine außergewöhnlichen Weitwinkel- und Nachtaufnahmen. Uns zeigt der junge Brite seine Tricks.</p>]]>
</content:encoded>
</item>
<item>
    <title>Kurz informiert: Anti-Corona-App, Open Library, Saudi Arabien, SpaceX</title>
    <link>https://www.heise.de/newsticker/meldung/Kurz-informiert-Anti-Corona-App-Open-Library-Saudi-Arabien-SpaceX-4693300.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Unser werktäglicher News-Überblick fasst die wichtigsten Nachrichten des Tages kurz und knapp zusammen.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693300</guid>
    <pubDate>Mon, 30 Mar 2020 18:55:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Kurz-informiert-Anti-Corona-App-Open-Library-Saudi-Arabien-SpaceX-4693300.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/0/8/3/1/Einzelbild-ee47406178918407.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Unser werktäglicher News-Überblick fasst die wichtigsten Nachrichten des Tages kurz und knapp zusammen.</p>]]>
</content:encoded>
</item>
<item>
    <title>Corona-Soforthilfe: Datenpanne bei der Investitionsbank Berlin</title>
    <link>https://www.heise.de/newsticker/meldung/Corona-Soforthilfe-Datenpanne-bei-der-Investitionsbank-Berlin-4693603.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Die Investitionsbank Berlin hat die Datenschutzbehörde über einen schwerwiegenden Programmierfehler auf der Antragsseite für Soforthilfen informiert.
]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693603</guid>
    <pubDate>Mon, 30 Mar 2020 18:46:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Corona-Soforthilfe-Datenpanne-bei-der-Investitionsbank-Berlin-4693603.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/1/1/3/__xfffd_berwachung-f865ec5e4b20278f.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Die Investitionsbank Berlin hat die Datenschutzbehörde über einen schwerwiegenden Programmierfehler auf der Antragsseite für Soforthilfen informiert.
</p>]]>
</content:encoded>
</item>
<item>
    <title>Microsoft Flight Simulator 2020 integriert Live-Traffic realer Flugzeuge</title>
    <link>https://www.heise.de/newsticker/meldung/Microsoft-Flight-Simulator-2020-integriert-Live-Traffic-realer-Flugzeuge-4693550.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Bei einer bestehenden Internetverbindung bildet der Flight Simulator 2020 die Echtzeit-Bewegungen realer Flugzeuge ab.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693550</guid>
    <pubDate>Mon, 30 Mar 2020 18:23:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Microsoft-Flight-Simulator-2020-integriert-Live-Traffic-realer-Flugzeuge-4693550.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/0/6/0/Master-1a0f50e245d7f2e9.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Bei einer bestehenden Internetverbindung bildet der Flight Simulator 2020 die Echtzeit-Bewegungen realer Flugzeuge ab.</p>]]>
</content:encoded>
</item>
<item>
    <title>Peter Schaar: Mit heißer Nadel gegen das Virus?</title>
    <link>https://www.heise.de/newsticker/meldung/Peter-Schaar-Mit-heisser-Nadel-gegen-das-Virus-4693535.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Wann sind Tracking-Apps im Kampf gegen die Coronavirus-Epidemie rechtlich verträglich? Ein Kommentar von Peter Schaar.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693535</guid>
    <pubDate>Mon, 30 Mar 2020 18:02:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Peter-Schaar-Mit-heisser-Nadel-gegen-das-Virus-4693535.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/1/0/4/5/Peter_Schaar-2528eb8a3c89572e.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Wann sind Tracking-Apps im Kampf gegen die Coronavirus-Epidemie rechtlich verträglich? Ein Kommentar von Peter Schaar.</p>]]>
</content:encoded>
</item>
<item>
    <title>heise+ | MIDI 2.0: Wie der neue MIDI-Standard neue Instrumente und Klänge realisiert</title>
    <link>https://www.heise.de/hintergrund/MIDI-2-0-Wie-der-neue-MIDI-Standard-neue-Instrumente-und-Klaenge-realisiert-4683675.html?wt_mc=rss.red.ho.ho.rdf.beitrag_plus.beitrag_plus</link>
    <description>
        <![CDATA[Nach knapp 40 Jahren wird der MIDI-Standard erstmals grundlegend überholt. Neuerungen von MIDI 2.0 könnten die Entwicklung elektronischer Instrumente beflügeln.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4683675</guid>
    <pubDate>Mon, 30 Mar 2020 17:00:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/hintergrund/MIDI-2-0-Wie-der-neue-MIDI-Standard-neue-Instrumente-und-Klaenge-realisiert-4683675.html?wt_mc=rss.red.ho.ho.rdf.beitrag_plus.beitrag_plus"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/02/2/8/6/2/6/9/9/07_linnstrum128-09d9f2ec1ccae542.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Nach knapp 40 Jahren wird der MIDI-Standard erstmals grundlegend überholt. Neuerungen von MIDI 2.0 könnten die Entwicklung elektronischer Instrumente beflügeln.</p>]]>
</content:encoded>
</item>
<item>
    <title>Radikaler Rückzug: O'Reilly steigt aus dem Geschäft mit Präsenzkonferenzen aus</title>
    <link>https://www.heise.de/developer/meldung/Radikaler-Rueckzug-O-Reilly-steigt-aus-dem-Geschaeft-mit-Praesenzkonferenzen-aus-4693221.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Nach 23 Jahren beendet O'Reilly überraschend seine Live-Events. Konferenzen wie die OSCON, Velocity und Strata Data hatten für die Branche Maßstäbe gesetzt.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693221</guid>
    <pubDate>Mon, 30 Mar 2020 16:56:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/developer/meldung/Radikaler-Rueckzug-O-Reilly-steigt-aus-dem-Geschaeft-mit-Praesenzkonferenzen-aus-4693221.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/0/7/7/0/shutterstock_424934146-7f65787b5314448c.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Nach 23 Jahren beendet O'Reilly überraschend seine Live-Events. Konferenzen wie die OSCON, Velocity und Strata Data hatten für die Branche Maßstäbe gesetzt.</p>]]>
</content:encoded>
</item>
<item>
    <title>WTF: Der Astrophysiker, dem man Neodym-Magnete aus der Nase ziehen musste</title>
    <link>https://www.heise.de/newsticker/meldung/Der-Astrophysiker-dem-man-Neodym-Magnete-aus-der-Nase-ziehen-musste-4693386.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Es gibt tolle Möglichkeiten, sich die Zeit in der Corona-Krise zu vertreiben. Magnete in der Nase gehören nicht dazu, stellte ein australischer Physiker fest.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693386</guid>
    <pubDate>Mon, 30 Mar 2020 16:24:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Der-Astrophysiker-dem-man-Neodym-Magnete-aus-der-Nase-ziehen-musste-4693386.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/0/9/0/1/shutterstock_130084862-9ef32a54a2584a2a.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Es gibt tolle Möglichkeiten, sich die Zeit in der Corona-Krise zu vertreiben. Magnete in der Nase gehören nicht dazu, stellte ein australischer Physiker fest.</p>]]>
</content:encoded>
</item>
<item>
    <title>TechStage | Media Markt, Saturn und Co: Die besten Deals zu Ostern 2020</title>
    <link>https://www.techstage.de/news/Cyberport-Lidl-Co-Die-besten-Deals-zu-Ostern-2020-4693345.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Xbox One mit vier Games für 289 Euro, Notebook plus Smartwatch für 600 Euro – die Oster-Deals 2020 sind gestartet. Wir haben die besten Angebote herausgesucht.]]>
    </description>
    <guid isPermaLink="true">https://techstage.de/-4693345</guid>
    <pubDate>Mon, 30 Mar 2020 16:00:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.techstage.de/news/Cyberport-Lidl-Co-Die-besten-Deals-zu-Ostern-2020-4693345.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/0/9/5/3/oster-deals_169-dd0e72f9bfce2c5a.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Xbox One mit vier Games für 289 Euro, Notebook plus Smartwatch für 600 Euro – die Oster-Deals 2020 sind gestartet. Wir haben die besten Angebote herausgesucht.</p>]]>
</content:encoded>
</item>
<item>
    <title>Google und Facebook investieren in Anzeigenmarkt</title>
    <link>https://www.heise.de/newsticker/meldung/Google-und-Facebook-investieren-in-Anzeigenmarkt-4693401.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Während Facebook Medienhäuser unterstützen will, in dem das Unternehmen Anzeigen kauft, stellt Google Plätze gratis zur Verfügung. ]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693401</guid>
    <pubDate>Mon, 30 Mar 2020 15:50:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Google-und-Facebook-investieren-in-Anzeigenmarkt-4693401.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/0/9/1/6/shutterstock_1469272925-e4a3096299e81266.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Während Facebook Medienhäuser unterstützen will, in dem das Unternehmen Anzeigen kauft, stellt Google Plätze gratis zur Verfügung. </p>]]>
</content:encoded>
</item>
<item>
    <title>Terrorismusbekämpfung: Zivilgesellschaft warnt EU vor Upload-Filtern</title>
    <link>https://www.heise.de/newsticker/meldung/Terrorismusbekaempfung-Zivilgesellschaft-warnt-EU-vor-Upload-Filtern-4693434.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[20 Organisationen warnen vor neuen EU-Auflagen gegen Terror: Die Corona-Krise zeige, dass automatisiertes Löschen von Inhalten übers Ziel hinausschieße.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693434</guid>
    <pubDate>Mon, 30 Mar 2020 15:46:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/newsticker/meldung/Terrorismusbekaempfung-Zivilgesellschaft-warnt-EU-vor-Upload-Filtern-4693434.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/0/9/4/5/shutterstock_637827229-8afdd6e60ab0dc9c.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>20 Organisationen warnen vor neuen EU-Auflagen gegen Terror: Die Corona-Krise zeige, dass automatisiertes Löschen von Inhalten übers Ziel hinausschieße.</p>]]>
</content:encoded>
</item>
<item>
    <title>heise-Angebot: Konferenzen: Minds Mastering Machines und enterPy auf November verschoben</title>
    <link>https://www.heise.de/developer/meldung/Konferenzen-Minds-Mastering-Machines-und-enterPy-auf-November-verschoben-4693135.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag</link>
    <description>
        <![CDATA[Die Konferenzen zu Machine Learning und Python finden im November statt, die enterPy startet darüber hinaus im Mai als virtuelle Konferenz.]]>
    </description>
    <guid isPermaLink="true">http://heise.de/-4693135</guid>
    <pubDate>Mon, 30 Mar 2020 15:22:00 +0200</pubDate>
    <content:encoded>
        <![CDATA[<p><a href="https://www.heise.de/developer/meldung/Konferenzen-Minds-Mastering-Machines-und-enterPy-auf-November-verschoben-4693135.html?wt_mc=rss.red.ho.ho.rdf.beitrag.beitrag"><img src="https://www.heise.de/scale/geometry/450/q80//imgs/18/2/8/7/0/7/1/7/m3_keynote-38c55485024775b3.jpeg" class="webfeedsFeaturedVisual" alt="" /></a></p><p>Die Konferenzen zu Machine Learning und Python finden im November statt, die enterPy startet darüber hinaus im Mai als virtuelle Konferenz.</p>]]>
</content:encoded>
</item>
</channel>
</rss>
"""
