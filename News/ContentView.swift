//
//  ContentView.swift
//  News
//
//  Created by Dimitri Brukakis on 31.03.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    var body: some View {
        TabView {
            SubscribedNewsFeedListView()
                .tabItem {
                    Image.list
                }
            
            Text("Second View")
            .tabItem {
                 Image(systemName: "2.square.fill")
                 Text("Second")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
