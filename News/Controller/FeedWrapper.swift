//
//  FeedWrapper.swift
//  News
//
//  Created by Dimitri Brukakis on 06.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import Foundation

class FeedWrapper<T>: Identifiable {
    
    enum FeedStatus {
        case unknown
        case loading
        case error
        case available
    }
    var id: String = UUID().uuidString
    var feed: T
    var status: FeedStatus = .unknown
    
    init (feed: T, status: FeedStatus = .unknown) {
        self.feed = feed
        self.status = status
    }
}
