//
//  NewsController.swift
//  News
//
//  Created by Dimitri Brukakis on 04.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import Foundation
import Combine
import XMLParsing

class NewsController: ObservableObject {
    
    @Published var feeds: [String:FeedWrapper<RSSFeed>] = [:]

    @Published var catalog: NewsFeedCatalog?
    
    var subscriptions: Set<AnyCancellable> = []
    
    init() {
        //
    }
    
    func loadFeed(url: URL) {
        feeds[url.absoluteString] = FeedWrapper<RSSFeed>(feed: RSSFeed(), status: .loading)
        URLSession.shared.dataTaskPublisher(for: url).tryMap { (data, response) in
            if let http = response as? HTTPURLResponse {
                precondition(http.statusCode == 200)
            }
            return data
        }.decode(type: RSSFeed.self, decoder: XMLDecoder()).catch { error in
            Just(RSSFeed())
        }.receive(on: DispatchQueue.main).sink { (feed) in
            self.feeds[url.absoluteString] = FeedWrapper<RSSFeed>(feed: feed, status: .available)
        }.store(in: &subscriptions)
    }
    
    func loadFeed(url: URL, completion: @escaping ((Result<RSSFeed, Error>) -> Void)) {
        feeds[url.absoluteString] = FeedWrapper<RSSFeed>(feed: RSSFeed(), status: .loading)
        URLSession.shared.dataTaskPublisher(for: url).tryMap { (data, response) in
            if let http = response as? HTTPURLResponse {
                precondition(http.statusCode == 200)
            }
            return data
        }.decode(type: RSSFeed.self, decoder: XMLDecoder()).catch { error in
            Just(RSSFeed())
        }.receive(on: DispatchQueue.main).sink { (feed) in
            self.feeds[url.absoluteString] = FeedWrapper<RSSFeed>(feed: feed, status: .available)
            completion(.success(feed))
        }.store(in: &subscriptions)
    }
    
    func loadFeedFromBundle(resource: String) {
        feeds[resource] = FeedWrapper<RSSFeed>(feed: RSSFeed(), status: .loading)
        if let filepath = Bundle.main.path(forResource: resource, ofType: "xml") {
            do {
                let content = try String(contentsOfFile: filepath)
                let feed = try RSSFeed.decode(from: content)
                feeds[resource]?.feed = feed
            } catch {
            }
        }
    }
    
    func update() {
        feeds.forEach {
            if let url = URL(string: $0.key) {
                loadFeed(url: url)
            }
        }
    }
    
    func configure(config: NewsFeedConfiguration) {
        config.feedURLs?.forEach { feeds[$0.absoluteString] = FeedWrapper<RSSFeed>(feed: RSSFeed()) }
    }
}

extension NewsController {
    static var preview: NewsController {
        let newsController = NewsController()
        newsController.loadFeedFromBundle(resource: "heise")
        newsController.loadFeedFromBundle(resource: "tagesschau-meldungen")
        newsController.loadFeedFromBundle(resource: "zdf-nachrichten")
        return newsController
    }
}
