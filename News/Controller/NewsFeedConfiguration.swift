//
//  NewsFeedConfiguration.swift
//  News
//
//  Created by Dimitri Brukakis on 12.07.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import Foundation
import Combine

class NewsFeedConfiguration {
    
    var subscribers = Set<AnyCancellable>()
    
    var feedURLs: [URL]?
    
    func loadFromResources() {
        ResourceProvider(resource: "NewsFeedCatalog", withExtension: "json").publisher
            .decode(type: NewsFeedCatalog.self, decoder: JSONDecoder())
            .sink(receiveCompletion: { (error) in
                print("\(error)")
            }, receiveValue: { value in
                self.feedURLs = value.feeds.compactMap { URL(string: $0.url) }
            }).store(in: &subscribers)
    }
}
