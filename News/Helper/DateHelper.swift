//
//  DateHelper.swift
//  News
//
//  Created by Dimitri Brukakis on 05.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import Foundation

class DateHelper {

    
    static func dateDifferanceAsText(date: Date, fromDate: Date = Date()) -> String {
        let cal = Calendar.current
        
        let years = cal.component(.year, from: fromDate) - cal.component(.year, from: date)
        if years == 1 { return .localized(key: "news.update.year") }
        if years > 1 { return .localized(key: "news.update.years", withIntValue: years) }

        let months = cal.component(.month, from: fromDate) - cal.component(.month, from: date)
        if months == 1 { return .localized(key: "news.update.month") }
        if months > 1 { return .localized(key: "news.update.months", withIntValue: months) }

        let days = cal.component(.day, from: fromDate) - cal.component(.day, from: date)
        if days == 1 { return .localized(key: "news.update.day") }
        if days > 1 { return .localized(key: "news.update.days", withIntValue: days) }

        let hours = cal.component(.hour, from: fromDate) - cal.component(.hour, from: date)
        if hours == 1 { return .localized(key: "news.update.hour") }
        if hours > 1 { return .localized(key: "news.update.hours", withIntValue: hours) }
        
        let minutes = cal.component(.minute, from: fromDate) - cal.component(.minute, from: date)
        if minutes == 1 { return .localized(key: "news.update.minute") }
        if minutes > 0 { return .localized(key: "news.update.minutes", withIntValue: minutes) }

        return .localized(key: "news.update.now")
    }
    
    
}
