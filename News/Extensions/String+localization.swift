//
//  String+localization.swift
//  News
//
//  Created by Dimitri Brukakis on 05.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import Foundation

extension String {
    
    static func localized(key: String) -> String {
        NSLocalizedString(key, comment: "")
    }
    
    static func localized(key: String, withIntValue value: Int) -> String {
        localized(key: key).replacingOccurrences(of: "{value}", with: String(value))
    }
}
