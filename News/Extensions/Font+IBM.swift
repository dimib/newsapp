//
//  Font+IBM.swift
//  News
//
//  Created by Dimitri Brukakis on 04.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import Foundation
import SwiftUI

extension Font {
    
    /// We need this fonts in the "News feed" tab
    static var newsFeedTitle: Font { Font.custom("IBMPlexMono-Light", size: 24.0) }
    static var newsFeedDescription: Font { Font.custom("IBMPlexSans-SemiBold", size: 16.0) }
    static var newsFeedLastUpdate: Font {
        Font.custom("IBMPlexSans-Regular", size: 12.0)
    }
    static var newsHeadline: Font { Font.custom("IBMPlexMono-Regular", size: 16.0) }
    
}
