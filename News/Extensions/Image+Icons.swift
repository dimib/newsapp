//
//  Image+Icons.swift
//  News
//
//  Created by Dimitri Brukakis on 04.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import Foundation
import SwiftUI

extension Image {
    
    /// Home image
    static var home: Image { Image(systemName: "house") }
    
    /// Feed list
    static var list: Image { Image(systemName: "list.bullet") }
    
    /// Update icon
    static var time: Image { Image(systemName: "clock") }
    
    /// Add another feed
    static var add: Image { Image(systemName: "plus.circle") }
    
    /// RSS Icon
    static var rssIcon: Image { Image("rssicon") }
}

