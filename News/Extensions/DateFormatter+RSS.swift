//
//  Date+RSS.swift
//  News
//
//  Created by Dimitri Brukakis on 05.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import Foundation


extension DateFormatter {
    
    var pubDateFormat: String { "E, dd MMM yyyy HH:mm:ss ZZZZ" }
    
    func parsePubDate(string: String) -> Date? {
        locale = Locale(identifier: "en_US_POSIX")
        dateFormat = pubDateFormat
        return date(from: string)
    }
    
    func formatPubDate(date: Date) -> String {
        locale = Locale(identifier: "en_US_POSIX")
        dateFormat = pubDateFormat
        return string(from: date)
    }
}

