//
//  RSSFeed.swift
//  News
//
//  Created by Dimitri Brukakis on 31.03.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import Foundation
import XMLParsing

class RSSFeed: Codable, Identifiable {
    var id: String { UUID().uuidString }
    var channel: RSSChannel?
    
    var title: String? { channel?.title }
    var description: String? { channel?.description }
    var updated: Date? {
        if let lastBuildDate = channel?.lastBuildDate {
            return DateFormatter().parsePubDate(string: lastBuildDate)
        }
        if let pubDate = channel?.pubDate {
            return DateFormatter().parsePubDate(string: pubDate)
        }
        return nil
    }
    
    var error: String?
    
    var items: [RSSItem] {
        channel?.items ?? []
    }
}

class RSSChannel: Codable {
    var title: String?
    var link: String?
    var description: String?
    var language: String?
    var pubDate: String?
    var lastBuildDate: String?
    var copyright: String?
    var category: String?
    var ttl: String?
    var image: RSSImage?
    var itunes_subtitle: String?
    var itunes_author: String?
    var itunes_summary: String?
    var itunes_owner: RSSItunesOwner?
    var itunes_explicit: String?
    var itunes_image: RSSItunesImage?
    var items: [RSSItem]?
    

    enum CodingKeys: String, CodingKey {
        case title, link, description, language
        case lastBuildDate, pubDate, copyright, category
        case ttl, image
        case itunes_subtitle = "itunes:subtitle"
        case itunes_author = "itunes:author"
        case itunes_summary = "itunes:summary"
        case itunes_owner = "itunes:owner"
        case itunes_explicit = "itunes:explicit"
        case itunes_image = "itunes:image"
        case items = "item"
    }
}

class RSSItem: Codable, Identifiable {
    var id: String { UUID().uuidString }
    var title: String?
    var link: String?
    var guid: String?
    var pubDate: String?
    var description: String?
    var image: RSSImage?
    var enclosure: RSSEnclosure?
    var content_encoded: String?
    var itunes_subtitle: String?
    var itunes_author: String?
    var itunes_summary: String?
    var itunes_duration: Int?
    
    init(title: String, description: String) {
        self.title = title
        self.description = description
        self.pubDate = DateFormatter().formatPubDate(date: Date())
    }
    
    var updated: Date? {
        if let pubDate = pubDate {
            return DateFormatter().parsePubDate(string: pubDate)
        }
        return nil
    }
        
    enum CodingKeys: String, CodingKey {
        case title, link, guid, pubDate, enclosure, image, description
        case content_encoded = "content:encoded"
        case itunes_subtitle = "itunes:subtitle"
        case itunes_author = "itunes:author"
        case itunes_summary = "itunes:summary"
        case itunes_duration = "itunes:duration"
    }
}

class RSSEnclosure: Codable {
    var url: String?
    var length: String?
    var type: String?
}
class RSSImage: Codable {
    var url: String?
    var title: String?
    var link: String?
}
class RSSItunesImage: Codable {
    var href: String
}
class RSSItunesOwner: Codable {
    var name: String?
    var email: String?
    enum CodingKeys: String, CodingKey {
        case name = "itunes:name"
        case email = "itunes:email"
    }
}


extension RSSFeed {
    
    static func decode(from data: Data) throws -> RSSFeed {
        let decoder = XMLDecoder()
        do {
            let rss = try decoder.decode(RSSFeed.self, from: data)
            return rss
        } catch {
            throw error
        }
    }
    
    static func decode(from string: String) throws -> RSSFeed {
        guard let data = string.data(using: .utf8) else {
            throw NewsError.invalid
        }
        return try decode(from: data)
    }
}
