//
//  NewsFeedCatalog.swift
//  News
//
//  Created by Dimitri Brukakis on 04.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import Foundation

/// We need this catalog as collection of available news feeds
struct NewsFeedCatalog: Codable {
    var feeds: [NewsFeedCatalogItem]
}

struct NewsFeedCatalogItem: Codable, Identifiable {
    enum FeedType: String, Codable {
        case rss = "rss"
        case atom = "atom"
    }
    
    var id: String = UUID().uuidString
    
    var type: FeedType
    var title: String
    var description: String
    
    var isPrivate: Bool
    var url: String
    var isSubscribed: Bool
    
    enum CodingKeys: String, CodingKey {
        case type, url, title, description
        case isPrivate = "private"
        case isSubscribed = "subscribed"
    }
}

extension NewsFeedCatalog {
    
    static func decode(from data: Data) throws -> NewsFeedCatalog {
        let decoder = JSONDecoder()
        do {
            let newsFeedCatalog = try decoder.decode(NewsFeedCatalog.self, from: data)
            return newsFeedCatalog
        } catch {
            throw error
        }
    }
    
    static func decode(from string: String) throws -> NewsFeedCatalog {
        guard let data = string.data(using: .utf8) else {
            throw NewsError.invalid
        }
        return try decode(from: data)
    }
}
