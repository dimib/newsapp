//
//  Error.swift
//  News
//
//  Created by Dimitri Brukakis on 04.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import Foundation

enum NewsError: Error {
    case invalid
    case notFound
}

