//
//  NewsListView.swift
//  News
//
//  Created by Dimitri Brukakis on 04.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import SwiftUI

struct NewsFeedListView: View {
    
    @EnvironmentObject var observableObject: NewsObservableObject
    
    var body: some View {
        NavigationView {
            List {
                ForEach(observableObject.newsFeeds) {
                    
                }
            }
            .navigationBarTitle("News")
            .navigationBarItems(trailing: Image.home)
        }
    }
}

struct NewsListView_Previews: PreviewProvider {
    static let observableObject = NewsObservableObject.sampleData
    static var previews: some View {
        NewsFeedListView()
    }
}
