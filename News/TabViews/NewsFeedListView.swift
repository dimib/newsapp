//
//  NewsFeedListView.swift
//  News
//
//  Created by Dimitri Brukakis on 05.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import SwiftUI

struct NewsFeedListView: View {
    
    @EnvironmentObject var newsController: NewsController
    
    var rssFeed: RSSFeed
    
    var body: some View {
        NavigationView {
            List {
                ForEach(rssFeed.items) { item in
                    HStack {
                        ListItem(rssItem: item)
                    }
                }
                .listRowInsets(.init(top: 10, leading: 16.0, bottom: 10, trailing: 16.0))
                .listStyle(PlainListStyle())
                .cornerRadius(10.0)
            }

            .navigationBarTitle(Text(rssFeed.title ?? ""), displayMode: .inline)
            //.navigationBarBackButtonHidden(true)
        }
        .onAppear() {
            UITableView.appearance().separatorStyle = .none
        }
    }
}

fileprivate struct ListItem: View {
    
    var rssItem: RSSItem
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(rssItem.title ?? "")
                .lineLimit(3)
                .font(.newsFeedTitle)
                .foregroundColor(Color.black)
                .padding(.top, 0.0)
                .padding(.leading, 0.0)
                .padding(.trailing, 0.0)
            Text(rssItem.description ?? "")
                .lineLimit(10)
                .font(.newsFeedDescription)
                .foregroundColor(Color.black)
                .padding(.leading, 0.0)
                .padding(.trailing, 0.0)
            if rssItem.updated != nil {
                HStack {
                    Image.time.resizable()
                        .frame(width: 10.0, height: 10.0)
                        .foregroundColor(Color.gray)
                    Text(DateHelper.dateDifferanceAsText(date: rssItem.updated ?? Date()))
                        .font(.newsFeedLastUpdate)
                        .foregroundColor(Color.gray)
                }
            } else {
                Spacer()
            }
        }
        .padding(.all, 4.0)
    }
}

struct NewsFeedListView_Previews: PreviewProvider {
    
    static let newsController = NewsController.preview
    static var previews: some View {
        NewsFeedListView(rssFeed: NewsController.preview.feeds.map({ ($0.value.feed ?? RSSFeed()) })[0]).environmentObject(newsController)
    }
}
