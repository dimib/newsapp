//
//  NewsListView.swift
//  News
//
//  Created by Dimitri Brukakis on 04.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import SwiftUI

struct SubscribedNewsFeedListView: View {
    
    @EnvironmentObject var newsController: NewsController
    
    var body: some View {
        NavigationView {
            List {
                ForEach(newsController.feeds.map({ $0.value })) { item in
                    HStack {
                        NavigationLink(destination: NewsFeedListView(rssFeed: item.feed)) {
                            ListItem(rssFeed: item.feed)
                        }
                    }
                }
                .listRowInsets(.init(top: 10, leading: 16.0, bottom: 10, trailing: 16.0))
                .listStyle(PlainListStyle())
                .cornerRadius(10.0)
            }
            .navigationBarTitle("news.feed")
            .navigationBarItems(trailing: AddButton())
        }
        .onAppear() {
            UITableView.appearance().separatorStyle = .none
        }
    }
}

fileprivate struct AddButton: View {
    var body: some View {
        NavigationLink(destination: NewsFeedCatalogListView()) {
            Image.add
        }.foregroundColor(.black)
    }
}

fileprivate struct ListItem: View {
    let rssFeed: RSSFeed
    
    var body: some View {
        HStack(alignment: .top, spacing: 9.0) {
            Image.rssIcon
                .resizable()
                .scaledToFit()
                .frame(width: 30.0, height: 30.0, alignment: .topTrailing)
                .padding(.top, 10.0)
            
            VStack(alignment: .leading) {
                Text(rssFeed.title ?? "")
                    .lineLimit(3)
                    .font(.newsFeedTitle)
                    .foregroundColor(Color.black)
                    .padding(.top, 0.0)
                    .padding(.leading, 0.0)
                    .padding(.trailing, 0.0)
                Text(rssFeed.description ?? "")
                    .lineLimit(10)
                    .font(.newsFeedDescription)
                    .foregroundColor(Color.black)
                    .padding(.leading, 0.0)
                    .padding(.trailing, 0.0)
                if rssFeed.updated != nil {
                    HStack {
                        Image.time.resizable()
                            .frame(width: 10.0, height: 10.0)
                            .foregroundColor(Color.gray)
                        Text(DateHelper.dateDifferanceAsText(date: rssFeed.updated ?? Date()))
                            .font(.newsFeedLastUpdate)
                            .foregroundColor(Color.gray)
                    }
                } else {
                    Spacer()
                }
            }
        }
    }
}

struct SubscribedNewsFeedListView_Previews: PreviewProvider {
    static var previews: some View {
        SubscribedNewsFeedListView().environmentObject(NewsController.preview)
    }
}

