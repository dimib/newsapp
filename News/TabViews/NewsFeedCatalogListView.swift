//
//  NewsFeedCatalogListView.swift
//  News
//
//  Created by Dimitri Brukakis on 06.04.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import SwiftUI

struct NewsFeedCatalogListView: View {
    @EnvironmentObject var newsController: NewsController
    
    var body: some View {
        NavigationView {
            List {
                ForEach(newsController.catalog?.feeds ?? []) { item in
                    ListItem()
                }
                .listRowInsets(.init(top: 10, leading: 16.0, bottom: 10, trailing: 16.0))
                .listStyle(PlainListStyle())
                .cornerRadius(10.0)
            }

            .navigationBarTitle(Text("hello"), displayMode: .inline)
            //.navigationBarBackButtonHidden(true)
        }
        .onAppear() {
            UITableView.appearance().separatorStyle = .none
        }
    }
}
fileprivate struct ListItem: View {
    var body: some View {
        Text("Hello")
    }
}

struct NewsFeedCatalogListView_Previews: PreviewProvider {
    static var previews: some View {
        NewsFeedCatalogListView()
    }
}
