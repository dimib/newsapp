//
//  NewsWidgetProvider.swift
//  News
//
//  Created by Dimitri Brukakis on 13.07.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import WidgetKit
import SwiftUI
import Intents

internal struct Provider: IntentTimelineProvider {
    
    typealias Entry = NewsEntry
    
    public func snapshot(for configuration: ConfigurationIntent, with context: Context, completion: @escaping (NewsEntry) -> ()) {
        let entry = NewsEntry(date: Date(), configuration: configuration, item: RSSItem(title: "Sample", description: "Preview Sample"))
        completion(entry)
    }

    public func timeline(for configuration: ConfigurationIntent, with context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [NewsEntry] = []
        
        let config = NewsFeedConfiguration()
        config.loadFromResources()
        
        guard let url = config.feedURLs?.first else {
            let timeline = Timeline(entries: entries, policy: .atEnd)
            completion(timeline)
            return
        }
        let newsController = NewsController()
        newsController.loadFeed(url: url) { result in
            let date = Date()
            var seconds = 0
            switch(result) {
            case let .success(feed):
                feed.items.forEach { (item) in
                    let entryDate = Calendar.current.date(byAdding: .second, value: seconds, to: date)
                    seconds += 10
                    let entry = NewsEntry(date: entryDate ?? Date(), configuration: configuration, item: item)
                    entries.append(entry)
                }
            case let .failure(error):
                break
            }

            let timeline = Timeline(entries: entries, policy: .atEnd)
            completion(timeline)
        }
    }
}

internal struct NewsEntry: TimelineEntry {
    public let date: Date
    public let configuration: ConfigurationIntent
    public let item: RSSItem
    public var url: URL {
        if let link = item.link, let url = URL(string: link) {
            return url
        }
        return URL(string: "http://www.google.de")!
    }
}
