//
//  NewsWidget.swift
//  NewsWidget
//
//  Created by Dimitri Brukakis on 12.07.20.
//  Copyright © 2020 Dimitri Brukakis. All rights reserved.
//

import WidgetKit
import SwiftUI
import Intents


struct PlaceholderView : View {
    var body: some View {
        NewsWidgetEntryView(entry: NewsEntry(date: Date(), configuration: ConfigurationIntent(), item: RSSItem(title: "Title", description: "Description")))
    }
}

struct NewsWidgetEntryView : View {
    var entry: Provider.Entry

    var body: some View {
        VStack(alignment: .leading) {
            HStack(alignment: .top) {
                Text(entry.item.title ?? "")
                    .font(.newsFeedTitle)
            }
            Text(entry.item.description ?? "")
                .font(.newsFeedDescription)
            HStack {
                Image.time.resizable()
                    .frame(width: 10.0, height: 10.0)
                    .foregroundColor(Color.gray)
                Text(DateHelper.dateDifferanceAsText(date: entry.item.updated ?? Date()))
                    .foregroundColor(Color.gray)
                    .font(.newsFeedLastUpdate)
                Image("rssicon")
                    .resizable()
                    .frame(width: 10, height: 10, alignment: .center)
            }
        }.padding(EdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10))
        .widgetURL(entry.url)
    }
}

@main
struct NewsWidget: Widget {
    private let kind: String = "NewsWidget"

    public var body: some WidgetConfiguration {
        IntentConfiguration(kind: kind, intent: ConfigurationIntent.self, provider: Provider(), placeholder: PlaceholderView()) { entry in
            NewsWidgetEntryView(entry: entry)
        }
        .configurationDisplayName("News Widget")
        .description("See the latest news on your spring board")
        .supportedFamilies([.systemMedium])
    }
}

struct NewsWidget_Previews: PreviewProvider {
    
    static var dummyItem: RSSItem {
        RSSItem(title: "Hello RSS", description: "This is just a sample RSS entry for previewing the News Widget")
    }
    
    static var previews: some View {
        NewsWidgetEntryView(entry: NewsEntry(date: Date(), configuration: ConfigurationIntent(), item: dummyItem))
            .previewContext(WidgetPreviewContext(family: .systemMedium))
    }
}
